
Script for monitoring a webpage example , the modules imported are being used for get , smtp and os (variables)

```py
import requests
import smtplib
import os

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')


response = requests.get('http://72-14-186-84.ip.linodeusercontent.com:8080/')
if False:
    print('Aplication is running successfully!')
else:
    print('Application is down. Fix it!')
    # send email to me
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        msg = "Subject: SITE DOWN\nFix the issue! Restart the application."
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)
```

NOTE: You need to create and app access in gmail for accounts with Two factor authentication 

URL: https://myaccount.google.com/apppasswords

Function for restart a container example
```py
def restart_server_and_container():
    # restart linode server
    print('Rebooting the server...')
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    nginx_server = client.load(linode_api4.Instance, 54681143)
    nginx_server.reboot()

    # restart application
    while True:
        nginx_server = client.load(linode_api4.Instance, 54681143)
        if nginx_server.status == 'running':
            time.sleep(5)
            restart_container()
            break
```

Function to send email notification

```py
def send_notification(email_msg):
    print('Sending an email')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        msg = f"Subject: SITE DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)
```

Function to restart a container , python example

```py
def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='72.14.186.84', username='root', key_filename='/home/radisys/.ssh/id_rsa')
    stdin, stdout, stderr = ssh.exec_command('docker start 6c4c87b1fc7e')
    print(stdout.readlines())
    ssh.close()
    print('Application restarted')
```

function to monitor an application *example*

```py
def monitor_application():
    try:
        response = requests.get('http://72-14-186-84.ip.linodeusercontent.com:8080/')
        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application is down. Fix it!')
            msg = f'Application returned {response.status_code}'
            send_notification(msg)
            restart_container()

    except Exception as ex:
        print(f'Connection error happened: {ex}')
        msg = 'Application not accessible at all'
        send_notification(msg)
        restart_server_and_container()

schedule.every(5).seconds.do(monitor_application)

while True:
     schedule.run_pending()
```

